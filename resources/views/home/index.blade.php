<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form method="POST" action="{{ route('home.store') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="text" name="nama" placeholder="Nama"><br/>
		<input type="text" name="alamat" placeholder="Alamat"><br/>
		<input type="submit" value="Simpan">
	</form>

	<br>

	<table>
		<thead>
			<tr>Nama</tr>
			<tr>Alamat</tr>
		</thead>
		<tbody>
			@foreach($data as $siswa)
				<tr>
					<td>{{ $siswa->nama }}</td>
					<td>{{ $siswa->alamat }}</td>
					<td>
						<form class="" action="{{ route('home.delete', $siswa->id) }}" method="post">{{ method_field('Delete') }}
							{{ csrf_field() }}
							<input type="submit" name="name" value="Delete">
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</html>
