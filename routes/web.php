<?php

Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home.index']);
Route::post('/', ['uses' => 'HomeController@store', 'as' => 'home.store']);
Route::delete('/delete/{id}',['uses'=>'HomeController@destroy', 'as'=>'home.delete']);
