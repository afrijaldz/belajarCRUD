<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;
use App\Siswa;

class HomeController extends Controller {

	protected $siswaModel;

	public function __construct(Siswa $siswa) {
		$this->siswaModel = $siswa;
	}

	public function index() {
		$siswa = $this->siswaModel->all();
		return view('home.index', ['data' => $siswa]);
	}

	public function store(Request $request) {
		$insertData = $this->siswaModel->create([
				'nama' => $request->input('nama'),
				'alamat' => $request->input('alamat')
			]);
		if($insertData) {
		return redirect()->back();
		}
	}
	public function destroy(Request $request, $id){
		$siswa = Siswa::where('id', $id)->first();

		if ($siswa) {
			$siswa->delete();
			return redirect()->back();
		}
	}
}
